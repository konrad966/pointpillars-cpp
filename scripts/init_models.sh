#/usr/bin/env bash

ONNX_2_TRT="../../onnx-tensorrt/build/onnx2trt"

mkdir -p model && cd model
gdown https://drive.google.com/uc?id=1gQWtBZ4vfrSmv2nToSIarr-d7KkEWqxw
gdown https://drive.google.com/uc?id=1dvUkjvhE0GEWvf6GchSGg8-lwukk7bTw
${ONNX_2_TRT} cbgs_pp_multihead_pfe.onnx -o cbgs_pp_multihead_pfe.trt -b 1 -d 16 
${ONNX_2_TRT} cbgs_pp_multihead_backbone.onnx -o cbgs_pp_multihead_backbone.trt -b 1 -d 16 

cd ../test/testdata
gdown https://drive.google.com/uc?id=1KD0LT0kzcpGUysUu__dfnfYnHUW62iwN
